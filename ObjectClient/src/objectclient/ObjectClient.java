/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objectclient;

/**
 *
 * @author liufulai
 */
import java.io.*;  
import java.net.ServerSocket;  
import java.net.Socket;  
import java.util.logging.Level;  
import java.util.logging.Logger;  

public class ObjectClient {

    /**
     * @param args the command line arguments
     */
    private final static Logger logger = Logger.getLogger(ObjectClient.class.getName()); 
    
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        for (int i = 0; i < 100; i++) {  
            Socket socket = null;  
            ObjectOutputStream os = null;  
            ObjectInputStream is = null;  
              
            try {  
                socket = new Socket("localhost", 10001);  
      
                os = new ObjectOutputStream(socket.getOutputStream());  
                User user = new User("user_" + i, "password_" + i);  
                System.out.println("user: " + user.getName() + "/" + user.getPassword());  
                os.writeObject(user);  
                os.flush();  
                  
                is = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));  
                Object obj = is.readObject();  
                if (obj != null) {  
                    user = (User)obj;  
                    System.out.println("Client: "+"user: " + user.getName() + "/" + user.getPassword());  
                }  
            } catch(IOException ex) {  
                logger.log(Level.SEVERE, null, ex);  
            } finally {  
                try {  
                    is.close();  
                } catch(Exception ex) {}  
                try {  
                    os.close();  
                } catch(Exception ex) {}  
                try {  
                    socket.close();  
                } catch(Exception ex) {}  
            }  
        } 
    }
    
}
