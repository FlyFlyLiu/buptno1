/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objectclient;


import java.io.*;  
import java.net.ServerSocket;  
import java.net.Socket;  
import java.util.logging.Level;  
import java.util.logging.Logger; 

/**
 *
 * @author liufulai
 */
public class ObjectServer1 {

    /**
     * @param args the command line arguments
     */
    private final static Logger logger = Logger.getLogger(ObjectServer1.class.getName()); 
    
    public static void main(String[] args) throws IOException{
        // TODO code application logic here
        ServerSocket server = new ServerSocket(10001);  
  
        while (true) {  
            Socket socket = server.accept();  
            invoke(socket);  
        }  
    }
    
    private static void invoke(final Socket socket) throws IOException {  
        new Thread(new Runnable() {  
            public void run() {  
                ObjectInputStream is = null;  
                ObjectOutputStream os = null;  
                try {  
                    is = new ObjectInputStream(new BufferedInputStream(socket.getInputStream())); 
                    os = new ObjectOutputStream(socket.getOutputStream());  
                    
                    User user1 = new User();
                    user1 = (User)is.readObject();  
                    System.out.println("SERVER: "+user1);
                    //User user = (User)user1;  
                    //System.out.println("Server: "+"user: " + user.getName() + "/" + user.getPassword());  
  
                    //user1.setName(user.getName() + "_new");  
                    //user1.setPassword(user.getPassword() + "_new");  
  
                    
                    os.writeObject(user1);  
                    os.flush();  
                } catch (IOException ex) {  
                    logger.log(Level.SEVERE, null, ex);  
                } catch(ClassNotFoundException ex) {  
                    logger.log(Level.SEVERE, null, ex);  
                } finally {  
                    try {  
                        is.close();  
                    } catch(Exception ex) {}  
                    try {  
                        os.close();  
                    } catch(Exception ex) {}  
                    try {  
                        socket.close();  
                    } catch(Exception ex) {}  
                }  
            }  
        }).start();  
    }
    
    
    
}
