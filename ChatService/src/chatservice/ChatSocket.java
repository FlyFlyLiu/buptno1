/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatservice;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.Socket;

/**
 *
 * @author liufulai
 * 
 * 
 */


public class ChatSocket extends Thread {
    
    Socket socket;//本地需要有socket来接受传入的s值
    
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        Socket socket = new Socket("localhost", 12345);
        new ChatSocket(socket).start();
    }
    
    
    
    public ChatSocket(Socket s){
        this.socket=s;
    }
    
    public void out(String out){
        try {
            // 执行数据的输出和相关功能的包装
            socket.getOutputStream().write(out.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public void run() {//run方法中加入接收数据的功能
        try {
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(
                            socket.getInputStream(),"UTF-8"));
            String line = null;
            while ((line = br.readLine())!=null) {//客户端的数据
                //发给聊天室的所有人
                ChatManager.getChatManager().publish(this, line);
            }
            br.close();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
