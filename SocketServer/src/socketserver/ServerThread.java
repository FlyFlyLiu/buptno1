/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socketserver;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author liufulai
 */
public class ServerThread extends Thread{
    
    Socket socket = null;
    
    public ServerThread (Socket socket){
        this.socket = socket;
    }
    
    //线程执行的操作，相应客户端的请求
    public void run(){
        
        try{
            DataInputStream input = new DataInputStream(socket.getInputStream());  
            String clientInputStr = input.readUTF();//这里要注意和客户端输出流的写方法对应,否则会抛 EOFException  
                
            // 处理客户端数据 
            System.out.println("客户端发过来的内容:" + clientInputStr);   
                  
            // 向客户端回复信息    
            DataOutputStream out = new DataOutputStream(socket.getOutputStream());
                String ServerOutput="null";
                if(clientInputStr.equalsIgnoreCase("one")){
                    ServerOutput = "return one";
                    out.writeUTF(ServerOutput);
                }
                else if(clientInputStr.equalsIgnoreCase("two")){
                    ServerOutput = "return two";
                    out.writeUTF(ServerOutput);
                }
                else if(clientInputStr.equalsIgnoreCase("q")){
                    ServerOutput = "close";
                    out.writeUTF(ServerOutput);
                }
                
                
            if(ServerOutput != null){
                out.writeUTF(ServerOutput);
                System.out.println("我已经回复了："+ ServerOutput);    
            }else{
                System.out.println("我没什么好回复的");  
            }
<<<<<<< HEAD
            
        //    firstInput.close();
         //   out1.close();
            
            
            
            
        }catch(IOException e){
=======
                
           out.close();   
           input.close();   

        }catch(Exception e){
>>>>>>> 051c2667db7928a481d4c9eae73f635d5d9ad363
            e.printStackTrace();
        }finally{
            try {
                if(socket!=null){
                    socket.close();
                }       
            } catch (Exception e) {              
                socket=null;                     
                System.out.println("服务端 finally 异常:" + e.getMessage());    
                
            }
        }
    }
           
}
